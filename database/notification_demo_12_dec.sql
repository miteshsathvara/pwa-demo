/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.35-MariaDB : Database - pwademo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pwademo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pwademo`;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2016_07_30_000002_create_notifications_table',1),
(4,'2016_08_11_191827_create_push_subscriptions_table',1);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(11) NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`type`,`notifiable_type`,`notifiable_id`,`data`,`read_at`,`created_at`,`updated_at`) values 
('0fc826ed-8f9d-460c-9b63-8265d8b4b51a','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T05:01:35+00:00\"}',NULL,'2018-12-07 05:01:35','2018-12-07 05:01:35'),
('11f4de08-f687-4462-ba08-f817aa964f7f','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T12:39:03+00:00\"}','2018-12-06 12:39:09','2018-12-06 12:39:03','2018-12-06 12:39:09'),
('19d30332-a8ad-4f39-a36c-6e2d3451fdea','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T07:59:05+00:00\"}',NULL,'2018-12-07 07:59:05','2018-12-07 07:59:05'),
('3bf35637-9be8-4f41-abd0-869af10eccad','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T12:41:34+00:00\"}','2018-12-06 12:44:10','2018-12-06 12:41:34','2018-12-06 12:44:10'),
('6940eb52-ae1b-4b2f-b3d9-8be91f2498a6','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T14:27:59+00:00\"}',NULL,'2018-12-06 14:27:59','2018-12-06 14:27:59'),
('6b40827a-d7b3-4dc2-871b-f43fcb26a09f','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T11:48:31+00:00\"}','2018-12-06 11:49:20','2018-12-06 11:48:31','2018-12-06 11:49:20'),
('7321f6e9-ffa1-4461-83dd-5bcd00f8061e','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T12:33:32+00:00\"}','2018-12-06 12:44:11','2018-12-06 12:33:32','2018-12-06 12:44:11'),
('8477829d-bf01-4875-86f3-d7f1b44d00eb','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T08:04:18+00:00\"}',NULL,'2018-12-07 08:04:18','2018-12-07 08:04:18'),
('89497a3e-501f-4861-920d-9c48c53109b8','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T04:46:49+00:00\"}',NULL,'2018-12-07 04:46:49','2018-12-07 04:46:49'),
('aed14290-332a-4740-a10a-5b3422a15829','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-12T13:06:15+00:00\"}',NULL,'2018-12-12 13:06:15','2018-12-12 13:06:15'),
('aeec4d7f-c972-4cf8-9ae6-9a1901230c57','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-12T13:06:20+00:00\"}',NULL,'2018-12-12 13:06:20','2018-12-12 13:06:20'),
('b79a5d7d-ad6e-4e57-a145-7d7558fcd1d7','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T11:49:01+00:00\"}','2018-12-06 11:49:19','2018-12-06 11:49:01','2018-12-06 11:49:19'),
('c997f9ce-2984-4d10-ae67-e84d50a4f964','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T11:48:45+00:00\"}','2018-12-06 11:49:19','2018-12-06 11:48:45','2018-12-06 11:49:19'),
('d7f1f27f-f948-470e-aabe-4c39e474f37c','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T07:33:15+00:00\"}',NULL,'2018-12-07 07:33:15','2018-12-07 07:33:15'),
('e3481f07-3500-4ed6-ac59-7a48fb552a4c','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-12T13:06:04+00:00\"}',NULL,'2018-12-12 13:06:04','2018-12-12 13:06:04'),
('e805c530-fc95-4ff2-856d-143c07c187d6','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T05:03:50+00:00\"}',NULL,'2018-12-07 05:03:50','2018-12-07 05:03:50'),
('f53422b7-d791-40b3-a2d2-a19c8c2ed629','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T11:49:23+00:00\"}','2018-12-06 11:49:32','2018-12-06 11:49:23','2018-12-06 11:49:32'),
('f7bab71e-1f0b-4eba-957d-b139775dfd9a','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-06T12:44:12+00:00\"}',NULL,'2018-12-06 12:44:12','2018-12-06 12:44:12'),
('f861fa63-6081-4c3f-83ea-0aeecbf97295','App\\Notifications\\HelloNotification','App\\User',1,'{\"title\":\"Hello from Laravel!\",\"body\":\"Thank you for using our application.\",\"action_url\":\"https:\\/\\/laravel.com\",\"created\":\"2018-12-07T08:04:53+00:00\"}',NULL,'2018-12-07 08:04:53','2018-12-07 08:04:53');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `push_subscriptions` */

DROP TABLE IF EXISTS `push_subscriptions`;

CREATE TABLE `push_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `endpoint` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `push_subscriptions_user_id_index` (`user_id`),
  CONSTRAINT `push_subscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `push_subscriptions` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'mitesh sathvara','mitesh.sathvara@knowarth.com','$2y$10$gsQgJ1DyebrTmUWI/r9QUeXa5zwZtp3P2YoffKKnSXzA1GnPFWzv2','fiNAt3IW1cNOcL7SySJF5MrCpP0RdzZwzrGnonttM2CsqL7aAEVrEDkIMb6o','2018-12-06 11:48:18','2018-12-06 11:48:18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
